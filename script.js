// let js = 'amazing' ;
// if (js === 'amazing') alert('Javascript is FUN!')

// var job = 'programmer';
// job = 'teacher';

// lastName = 'Schmendmann';
// console.log(lastName);

//MATH OPERATORS

// const now = 2037;
// const ageJonas = now - 1991;
// const ageSarah = now - 2018;
// console.log(ageJonas, ageSarah)

// console.log(ageJonas * 2, ageJonas/10,2 * * 3)

// const firstName = 'Jonas';
// const lastName = "Schmendmann";
// console.log(firstName + ' ' + lastName);

//ASSIGNMENT OPERATORS

// let x = 10 + 5;
// x + = 10; //x=x+10=25
// x + = 4; //x=x*4=100
// x + +; //x=x+1
// x - -;
// x - -;
// console.log(x);

// COMPARISON OPERATORS (>,<,>=,<=)

// console.log(ageJonas > ageSarah);
// console.log(ageSarah >= 18);

// const isFullAge = ageSarah >= 18;
// console.log(now - 1991 > now - 2018);

// let x,y;
// x = y = 25 - 10 - 5;
// console.log(x,y);

// const averageAge = (ageJonas + ageSarah)/2;
// console.log(ageJonas, ageSarah, averageAge);

/////CODING CHALLENGE 1/////

/*
Mark and John are trying to compare their BMI (body mass index), which is calculated
using the formula:
BMI = mass / height ** 2 = mass / (height * height).
(mass in kg and height in meter)

1. Store Mark's and John's mass and height in variables.
2. Calculate both their BMI's using the formula (you can even implement both versions)
3. Create a boolean variable 'markHigherBMI' containing information about whether Mark has
higher BMI than John.

TEST DATA 1: Marks weights 78 kg and is 1.69 m tall.
John weights 92 kg and is 1.95 m tall.

TEST DATA 2: Marks weights 95 kg and is 1.88 m tall.
John weights 85 kg and is 1.75 m tall.
*/

// TEST DATA 1

// const massMark = 78;
// const heightMark = 1.69;
// const massJohn = 92;
// const heightJohn = 1.95;

// TEST DATA 2

// const massMark = 95;
// const heightMark = 1.88;
// const massJohn = 85;
// const heightJohn = 1.75;

// const BMIMark = massMark / heightMark ** 2;
// const BMIJohn = massJohn / (heightJohn * heightJohn);
// const markHigherBMI = BMIMark > BMIJohn;
// console.log(BMIMark, BMIJohn, markHigherBMI);

/////CHALLENGE 1 DONE/////

// STRINGS AND TEMPLATES LITERALS

// const firstName = 'Jonas';
// const job = 'teacher';
// const birthYear = 1991;
// const year = 2037;

// const jonas = "I'm " + firstName + ', a ' + (year - birthYear) + ' year old ' + job + '!';
// console.log(jonas);

// const jonasNew = `I'm ${firstName}, a ${year - birthYear} year old ${job}!`;   
// console.log(jonasNew);

// console.log(`Just a regular string`)

// console.log('String with \n\
// multiple \n\
// lines');

// console.log(`String with
// multiple
// lines`);

// const age = 15;

// if(age >= 18) {
//     console.log('Sarah can start driving license🚘')
// } else {
//     const yearsLeft = 18 - age;
//     console.log(`Sarah is too young. Wait another ${yearsLeft} years :)`);
// }

// const birthYear = 1998;

// let century;
// if(birthYear <= 2000) {
//     century = 20;
// } else {
//     century = 21;
// }
// console.log(century);

/////CODING CHALLENGE 2/////

/*
Use the BMI example from challenge #1, and the code you already wrote, and improve it:

1. Print a nice output to the console, saying who has the higher BMI. The message can be either "Mark's 
BMI is higher than John's!" or "John's BMI is higher than Mark's!"
2. Use a template literal to include the BMI values in the outputs. Example:
"Mark's BMI (28.3) is higher than John's (23.9)!"

HINT: Use an if/else statement
*/

//TEST DATA 1

// const massMark = 78;
// const heightMark = 1.69;
// const massJohn = 92;
// const heightJohn = 1.95;

//TEST DATA 2

// const massMark = 95;
// const heightMark = 1.88;
// const massJohn = 85;
// const heightJohn = 1.75;

// const BMIMark = massMark / heightMark ** 2;
// const BMIJohn = massJohn / (heightJohn * heightJohn);
// console.log(BMIMark, BMIJohn);

// if(BMIMark > BMIJohn) {
//     console.log(`Mark's BMI (${BMIMark}) is higher than John's (${BMIJohn})!`)
// } else {
//     console.log(`John's BMI (${BMIJohn}) is higher than Mark's (${BMIMark})!`)
// }

// if(BMIMark > BMIJohn) {
//     console.log("Mark's BMI is higher than John's!")
// } else {
//     console.log("John's BMI is higher than Mark's!")
// }

//TYPE CONVERSION

// const inputYear = '1991';
// console.log(Number(inputYear));
// console.log(Number(inputYear) + 18); // Reikia rasyti 'number', kad skaicius pasiverstu is stringo i skaiciu    

// console.log(Number('Jonas'));
// console.log(typeof NaN); //NOT A NUMBER (NaN)

// console.log(String(23), 23);

//TYPE COERCION

// console.log('I am ' + 23 + ' years old');
// console.log('23' - '10' - 3);
// console.log('23' * '2');
// console.log('23' / '2');
// console.log('23' > '18');

// let n = '1' + 1;//=11, nes 1 yra stringas
// n = n - 1;//11-1=10
// console.log(n);

// 2 + 3 + 4 + '5' = 95, nes 9+'5', susidedam skaicius
// '10' - '4' - '3' - 2 + '5' = 15, nes 10-4=6-3=3-2=1 + '5', susidedam skaicius

//TRUTHY AND FALSY VALUES

//5 falsy values: 0, '', undefined, null, NaN

// console.log(Boolean(0));  //false
// console.log(Boolean(undefined));  //false
// console.log(Boolean('Jonas'));  //true
// console.log(Boolean({}));  //true
// console.log(Boolean(''));  //false

// const money = 100;
// if(money) {
//     console.log("Don't spend it all ;)"); 
// } else {
//     console.log('You should get a job!');  //prints 'You should get a job!', when const money = 0
// }

// let height =12345;
// if(height) {
//     console.log('YAY! Height is defined');  //prints 'Height is defined' when let height = 12345 
// } else {
//     console.log('Height is UNDEFINED');  //prints 'Height is undefined' when let height; doesn't have a value, when 0, height is undefined
// }


// const age = 18; //when string '18', then console shows nothing
// if(age === 18) console.log('You just became an adult (strict)');

// if(age == 18) console.log('You just became an adult (loose)');

// '18' == 18  //true
// '18' === 18  //false

// const favourite = prompt("What's your favourite number?");
// console.log(favourite);
// console.log(typeof favourite);  //checking the type(string, boolean, number...)

// if(favourite == 23) {  // '23' == 23, but if ===, then doesn't work
//     console.log('Cool! 23 is an amazing number :)');
// } else if (favourite === 7){
//     console.log('7 is also a cool number');
// } else if (favourite === 9){
//     console.log('9 is also a cool number');
// } else {
//     console.log('Number is not 23 or 7 or 9');
// }

// if(favourite !== 23 ) console.log('Why not 23?');

// Boolean logic(Basic boolean logic:THE AND, OR & NOT OPERATORS)

// EXAMPLE: (Boolean variables that can be either TRUE or FALSE)
// A: Sarah has a driver's license
// B: Sarah has good vision

// A AND B:
// "Sarah has a driver's license AND good vision"

// AND    TRUE    FALSE (possible values)
// TRUE   TRUE    FALSE
// FALSE  FALSE   FALSE (true when all are true, no matter how many variables)

// A OR B:
// "Sarah has a driver's license OR good vision"

// OR     TRUE    FALSE
// TRUE   TRUE    TRUE
// FALSE  TRUE    FALSE (true when one is true)

// NOT A, NOT B:
// Inverts true/false value (if false, then true, if true, then false)

//Boolean variables (age = 16):
// A: Age is greater or equal (false)
// B: Age is less than 30 (true)

// LET'S USE OPERATORS
// !A(false) TRUE
// A(false) AND B(true) FALSE
// A(false) OR B(true) TRUE
// !A(true) AND B(true) TRUE 
// A(false) OR !B(false) FALSE

// const hasDriversLicense = true; //A
// const hasGoodVision = true; //B

// console.log(hasDriversLicense && hasGoodVision); //&&-and
// console.log(hasDriversLicense || hasGoodVision); //||-or
// console.log(!hasDriversLicense); //!-not

// const shouldDrive = hasDriversLicense && hasGoodVision; 

// if(hasDriversLicense && hasGoodVision){
//     console.log('Sarah is able to drive!');
// } else {
//     console.log('Someone else should drive...');
// }

// const isTired = true; //C
// console.log(hasDriversLicense && hasGoodVision && isTired); //||-or

// if(hasDriversLicense && hasGoodVision && !isTired){
//     console.log('Sarah is able to drive!');
// } else {
//     console.log('Someone else should drive...');
// }

/////CODING CHALLENGE 3/////

/*
There are two gymnastics teams, Dolphins and Koalas. They compete against each other 3 times. 
The winner with the highest average score wins the a trophy!

1. Calculate the average score for each team, using the test data below
2. Compare the team's average scores to determine the winner of the competition, 
and print it to the console. Don't forget that there can be a draw, so test for that as well 
(draw means they have the same average score).

3. BONUS 1: Include a requirement for a minimum score of 100. With this rule, a team only wins 
if it has a higher score than the other team, and the same time a score of at least 100 points. 
HINT: Use a logical operator to test for minimum score, as well as multiple else-if blocks 😉
4. BONUS 2: Minimum score also applies to a draw! So a draw only happens when both teams have the same 
score and both have a score greater or equal 100 points. Otherwise, no team wins the trophy.

TEST DATA: Dolphins score 96, 108 and 89. Koalas score 88, 91 and 110
TEST DATA BONUS 1: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 123
TEST DATA BONUS 2: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 106

GOOD LUCK 😀
*/

const scoreDolphins = (96 + 108 + 89) / 3;
const scoreKoalas = (88 + 91 + 110) / 3;
console.log(scoreDolphins, scoreKoalas);

if(scoreDolphins > scoreKoalas){
    console.log('Dolphins win the trophy');
} else if(scoreKoalas > scoreDolphins){
    console.log('Koalas win the trophy');
} else if(scoreDolphins === scoreKoalas){
    console.log('Both win the trophy');
}













